# How use

1. Push updates to repository
2. Create new image (docker build --no-cache -t vsoft-meeting:VERSION .)
3. Push image to Docker hub
4. Update docker-compose with new version
5. Run

Access https://yourip:3000


# Repo base
http://github.com/havfo/multiparty-meeting/

# Thanks