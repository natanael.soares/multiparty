FROM node:10-alpine AS build

ARG DIR=/project
ARG PROJECT=/multiparty

RUN apk add --no-cache git bash

WORKDIR ${DIR}
RUN git clone --single-branch --branch master https://gitlab.com/natanael.soares/multiparty.git

ARG APP=${DIR}${PROJECT}/app
ARG SERVER=${DIR}${PROJECT}/server

WORKDIR ${APP}

RUN npm install
RUN mkdir -p ${SERVER}/public 
RUN npm run build

WORKDIR ${SERVER}

RUN apk add --no-cache git build-base python linux-headers
RUN npm install

FROM node:10-alpine

ARG DIR=/project
ARG PROJECT=/multiparty

COPY --from=build ${DIR}${PROJECT}/server ${DIR}${PROJECT}/server

EXPOSE 3000 
EXPOSE 40000-49999/udp

ENV SERVER ${DIR}${PROJECT}/server

COPY start.sh /
CMD ["/start.sh"]