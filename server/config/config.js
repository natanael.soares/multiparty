const os = require('os');

module.exports =
{

	// Auth conf
	auth :
	{
		lti :
		{
			consumerKey    : 'key',
			consumerSecret : 'secret'
		},
		// oidc:
		// {
		// 	// The issuer URL for OpenID Connect discovery
		// 	// The OpenID Provider Configuration Document
		// 	// could be discovered on:
		// 	// issuerURL + '/.well-known/openid-configuration'

		// 	issuerURL     : 'https://127.0.0.1'+'/.well-known/openid-configuration',
		// 	clientOptions :
		// 	{
		// 		client_id     : 's',
		// 		client_secret : 's',
		// 		scope       		: 'openid email profile',
		// 		// where client.example.com is your multiparty meeting server
		// 		redirect_uri  : 'https://client.example.com/auth/callback'
		// 	}

		// }
	},
	redisOptions : {
		port: 6379,
		host: 'redis'
	},
	// session cookie secret
	cookieSecret : 'T0P-S3cR3t_cook!e',
	cookieName   : 'multiparty-meeting.sid',
	tls          :
	{
		cert : `${__dirname}/../certs/mediasoup-demo.localhost.cert.pem`,
		key  : `${__dirname}/../certs/mediasoup-demo.localhost.key.pem`
	},
	// Listening port for https server.
	listeningPort         : 3000,
	// Any http request is redirected to https.
	// Listening port for http server. 
	listeningRedirectPort : 8090,
	// Listens only on http, only on listeningPort
	// listeningRedirectPort disabled
	// use case: loadbalancer backend
	httpOnly              : true,
	// If this is set to true, only signed-in users will be able
	// to join a room directly. Non-signed-in users (guests) will
	// always be put in the lobby regardless of room lock status.
	// If false, there is no difference between guests and signed-in
	// users when joining.
	requireSignInToAccess : false,
	// This flag has no effect when requireSignInToAccess is false
	// When truthy, the room will be open to all users when the first
	// authenticated user has already joined the room.
	activateOnHostJoin    : true,
	// Mediasoup settings
	mediasoup             :
	{
		numWorkers : Object.keys(os.cpus()).length,
		// mediasoup Worker settings.
		worker     :
		{
			logLevel : 'warn',
			logTags  :
			[
				'info',
				'ice',
				'dtls',
				'rtp',
				'srtp',
				'rtcp'
			],
			rtcMinPort : 40000,
			rtcMaxPort : 49999
		},
		// mediasoup Router settings.
		router :
		{
			// Router media codecs.
			mediaCodecs :
			[
				{
					kind      : 'audio',
					mimeType  : 'audio/opus',
					clockRate : 48000,
					channels  : 2
				},
				{
					kind       : 'video',
					mimeType   : 'video/VP8',
					clockRate  : 90000,
					parameters :
					{
						'x-google-start-bitrate' : 1000
					}
				},
				{
					kind       : 'video',
					mimeType   : 'video/VP9',
					clockRate  : 90000,
					parameters :
					{
						'profile-id'             : 2,
						'x-google-start-bitrate' : 1000
					}
				},
				{
					kind       : 'video',
					mimeType   : 'video/h264',
					clockRate  : 90000,
					parameters :
					{
						'packetization-mode'      : 1,
						'profile-level-id'        : '4d0032',
						'level-asymmetry-allowed' : 1,
						'x-google-start-bitrate'  : 1000
					}
				},
				{
					kind       : 'video',
					mimeType   : 'video/h264',
					clockRate  : 90000,
					parameters :
					{
						'packetization-mode'      : 1,
						'profile-level-id'        : '42e01f',
						'level-asymmetry-allowed' : 1,
						'x-google-start-bitrate'  : 1000
					}
				}
			]
		},
		// mediasoup WebRtcTransport settings.
		webRtcTransport :
		{
			listenIps :
			[
				// change ip to your servers IP address!
				{ ip: '177.180.244.197', announcedIp: null }

				// Can have multiple listening interfaces
				// { ip: '::/0', announcedIp: null }
			],
			initialAvailableOutgoingBitrate : 1000000,
			minimumAvailableOutgoingBitrate : 600000,
			// Additional options that are not part of WebRtcTransportOptions.
			maxIncomingBitrate              : 1500000
		}
	}
};
